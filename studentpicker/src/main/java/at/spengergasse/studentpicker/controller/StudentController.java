package at.spengergasse.studentpicker.controller;

import at.spengergasse.studentpicker.model.Student;
import at.spengergasse.studentpicker.service.StudentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.Optional;

@RestController
@RequestMapping("/student")
public class StudentController {

    @Autowired
    private StudentService studentService;

    @GetMapping("/all")
    @ResponseBody
    public Iterable<Student> all() {return studentService.all();}

    @GetMapping("/random")
    @ResponseBody
    public Optional<Student> random() {return studentService.random();}
}
