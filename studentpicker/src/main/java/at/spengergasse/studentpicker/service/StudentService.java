package at.spengergasse.studentpicker.service;

import at.spengergasse.studentpicker.model.Student;
import at.spengergasse.studentpicker.repository.StudentRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class StudentService {

    @Autowired
    private StudentRepository studentRepository;

    public Iterable<Student> all() {return studentRepository.findall();}

    public Optional<Student> random() {return studentRepository.findrandom(); }

}
