package at.spengergasse.studentpicker.repository;

import at.spengergasse.studentpicker.model.Student;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface StudentRepository extends{

    Iterable<Student> findall();
    //Optional<Student> rondom();

    Optional<Student> findrandom();
}
