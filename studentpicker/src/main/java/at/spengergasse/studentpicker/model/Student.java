package at.spengergasse.studentpicker.model;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Id;
import java.util.Date;

public class Student {

    @Id
    private Long id;

    @Getter
    @Setter
    private String firstname;

    @Getter
    @Setter
    private String lastnamme;

    @Getter
    @Setter
    private String classname;

    @Getter
    @Setter
    private Date dateofbirth;


}
