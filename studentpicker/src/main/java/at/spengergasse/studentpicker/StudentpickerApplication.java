package at.spengergasse.studentpicker;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class StudentpickerApplication {

    public static void main(String[] args) {
        SpringApplication.run(StudentpickerApplication.class, args);
    }
}
