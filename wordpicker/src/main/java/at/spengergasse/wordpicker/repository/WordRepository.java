package at.spengergasse.wordpicker.repository;

import at.spengergasse.wordpicker.model.Word;

import java.util.Optional;

public interface WordRepository {
    Iterable<Word> findall();

    Optional<Word> findrandom();
}
