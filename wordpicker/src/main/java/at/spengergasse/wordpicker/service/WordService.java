package at.spengergasse.wordpicker.service;

import at.spengergasse.wordpicker.model.Word;
import at.spengergasse.wordpicker.repository.WordRepository;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Optional;


public class WordService {
    @Autowired
    private WordRepository wordRepository;

    public Iterable<Word> all() {return wordRepository.findall();}

    public Optional<Word> random() {return wordRepository.findrandom();}
}