package at.spengergasse.wordpicker.model;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Id;

public class Word {

    @Id
    private Long id;

    @Getter
    @Setter
    private int length;
}
