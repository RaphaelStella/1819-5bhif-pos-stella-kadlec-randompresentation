package at.spengergasse.wordpicker.controller;


import at.spengergasse.wordpicker.model.Word;
import at.spengergasse.wordpicker.service.WordService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.Optional;

@RestController
@RequestMapping("/word")
public class WordController {

    @Autowired
    private WordService wordService;

    @GetMapping("/all")
    @ResponseBody
    public Iterable<Word> all() {return wordService.all();}

    @GetMapping("/random")
    @ResponseBody
    public Optional<Word> random() {return wordService.random();}

}
