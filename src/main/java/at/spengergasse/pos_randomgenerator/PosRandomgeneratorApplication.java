package at.spengergasse.pos_randomgenerator;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PosRandomgeneratorApplication {

    public static void main(String[] args) {
        SpringApplication.run(PosRandomgeneratorApplication.class, args);
    }
}
