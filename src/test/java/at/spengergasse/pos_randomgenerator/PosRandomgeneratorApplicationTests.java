package at.spengergasse.pos_randomgenerator;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class PosRandomgeneratorApplicationTests {

    @Test
    public void contextLoads() {
    }

}
