package at.spengergasse.topicpicker;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TopicpickerApplication {

    public static void main(String[] args) {
        SpringApplication.run(WordpickerApplication.class, args);
    }
}
