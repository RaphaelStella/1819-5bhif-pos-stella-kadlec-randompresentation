package at.spengergasse.topicpicker.service;

import at.spengergasse.topicpicker.model.Topic;
import at.spengergasse.topicpicker.repository.TopicRepository;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Optional;

public class TopicService {
    @Autowired
    private TopicRepository topicRepository;

    public Iterable<Topic> all() {return topicRepository.findall();}

    public Optional<Topic> random() {return topicRepository.findrandom();}
}
