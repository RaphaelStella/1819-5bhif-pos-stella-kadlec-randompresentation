package at.spengergasse.topicpicker.repository;

import at.spengergasse.topicpicker.model.Topic;

import java.util.Optional;

public interface TopicRepository {

    Iterable<Topic> findall();
    //Optional<Student> rondom();

    Optional<Topic> findrandom();
}
