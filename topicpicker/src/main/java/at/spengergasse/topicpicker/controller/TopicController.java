package at.spengergasse.topicpicker.controller;

import at.spengergasse.topicpicker.model.Topic;
import at.spengergasse.topicpicker.service.TopicService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.Optional;

@RestController
@RequestMapping("/topic")

public class TopicController {

        @Autowired
        private TopicService topicService;

        @GetMapping("/all")
        @ResponseBody
        public Iterable<Topic> all() {return topicService.all();}

        @GetMapping("/random")
        @ResponseBody
        public Optional<Topic> random() {return topicService.random();}

}
