package at.spengergasse.topicpicker.model;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Id;

public class Topic {

    @Id
    private Long id;

    @Getter
    @Setter
    private String name;

    @Getter
    @Setter
    private int difficulty;

    @Getter
    @Setter
    private double duration;
}
